package hail_test

import (
	"errors"
	"testing"

	"gitlab.com/JankTech/hail"
)

func TestRegex(t *testing.T) {
	t.Parallel()
	t.Run("correct pattern passes", func(t *testing.T) {
		t.Parallel()

		v := "abc-123"
		r, err := hail.Rule(hail.Regex(`abc-*`))
		if err != nil {
			panic(err)
		}

		ok, errs := r.Validate(v)
		if !ok {
			t.Fatalf("unexpected ok value: expected true, got false")
		}
		if len(errs) > 0 {
			t.Fatalf("incorrect validation error count: expected %d got %d", 0, len(errs))
		}
	})
	t.Run("incorrect pattern fail", func(t *testing.T) {
		t.Parallel()

		v := "abc-123"
		r, err := hail.Rule(hail.Regex(`xyz-*`))
		if err != nil {
			panic(err)
		}

		ok, errs := r.Validate(v)
		if ok {
			t.Fatalf("unexpected ok value: expected false, got true")
		}
		if len(errs) != 1 {
			t.Fatalf("incorrect validation error count: expected %d got %d", 1, len(errs))
		}

		var e hail.ErrRegex
		if !errors.As(errs[0], &e) {
			t.Fatalf("incorrect error type: expected %T, got %T", e, errs[0])
		}
	})
}
