package hail

import (
	"errors"
	"fmt"
)

func PrintableASCII() func(value string) error {
	f := Regex(`^[ -~]*$`)
	return func(value string) error {
		err := f(value)
		var errRegex ErrRegex
		if errors.As(err, &errRegex) {
			return ErrASCII{value}
		}
		return nil
	}
}

type ErrASCII struct {
	value string
}

func NewErrASCII(value string) ErrASCII {
	return ErrASCII{value: value}
}

func (err ErrASCII) Error() string {
	return fmt.Sprintf("value %s contains characters outside of the ASCII printable range", err.value)
}
