package hail

import (
	"fmt"
	"strings"
)

var acceptedStrings []string = []string{"yes", "on"}

// The value under validation must be "yes" or "on".
// This is useful for validating checkbox/boolean html form fields such as "Terms of Service" acceptance.
func Accepted() func(value string) error {
	return func(value string) error {
		for _, s := range acceptedStrings {
			if value == s {
				return nil
			}
		}
		return ErrAccepted{value: value}
	}
}

type ErrAccepted struct {
	value string
}

func NewErrAccepted(value string) ErrAccepted {
	return ErrAccepted{
		value: value,
	}
}

func (err ErrAccepted) Error() string {
	return fmt.Sprintf("value %v was not one of [%s]", err.value, strings.Join(acceptedStrings, " | "))
}
