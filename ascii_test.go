package hail_test

import (
	"fmt"
	"testing"

	"gitlab.com/JankTech/hail"
)

var printableASCII []string = []string{
	` 0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!"$%^&*()_+-=|{}[];'#:@~"<>?,./\|`,
}

var printableNonASCII []string = []string{
	`£`,
	`€`,
	`Œ`,
}

var nonPrintableASCII []string = []string{
	string([]byte{0}),
	string([]byte{1}),
	string([]byte{2}),
	string([]byte{3}),
	string([]byte{4}),
	string([]byte{5}),
	string([]byte{6}),
	string([]byte{7}),
	string([]byte{8}),
	string([]byte{9}),
	string([]byte{10}),
	string([]byte{11}),
	string([]byte{12}),
	string([]byte{13}),
	string([]byte{14}),
	string([]byte{15}),
	string([]byte{16}),
	string([]byte{17}),
	string([]byte{18}),
	string([]byte{19}),
	string([]byte{20}),
	string([]byte{21}),
	string([]byte{22}),
	string([]byte{23}),
	string([]byte{24}),
	string([]byte{25}),
	string([]byte{26}),
	string([]byte{27}),
	string([]byte{28}),
	string([]byte{29}),
	string([]byte{30}),
	string([]byte{31}),
}

func TestASCII(t *testing.T) {
	t.Parallel()
	for _, a := range printableASCII {
		a := a
		t.Run(fmt.Sprintf(`%s passes`, a), func(t *testing.T) {
			t.Parallel()
			a := a
			r, err := hail.Rule(hail.PrintableASCII())
			if err != nil {
				panic(err)
			}

			ok, errs := r.Validate(a)
			if !ok {
				t.Fatalf("unexpected ok value: expected true, got false")
			}
			if len(errs) > 0 {
				t.Fatalf("incorrect validation error count: expected %d got %d", 0, len(errs))
			}
		})
	}

	for _, a := range printableNonASCII {
		a := a
		t.Run(fmt.Sprintf("%s fails", a), func(t *testing.T) {
			t.Parallel()
			a := a
			r, err := hail.Rule(hail.PrintableASCII())
			if err != nil {
				panic(err)
			}

			ok, errs := r.Validate(a)
			if ok {
				t.Fatalf("unexpected ok value: expected false, got true")
			}
			if len(errs) != 1 {
				t.Fatalf("incorrect validation error count: expected %d got %d", 1, len(errs))
			}
		})
	}

	for _, a := range nonPrintableASCII {
		a := a
		t.Run(fmt.Sprintf("%x fails", a), func(t *testing.T) {
			t.Parallel()
			a := a
			r, err := hail.Rule(hail.PrintableASCII())
			if err != nil {
				panic(err)
			}

			ok, errs := r.Validate(a)
			if ok {
				t.Fatalf("unexpected ok value: expected false, got true")
			}
			if len(errs) != 1 {
				t.Fatalf("incorrect validation error count: expected %d got %d", 1, len(errs))
			}
		})
	}
}
