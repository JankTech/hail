package hail_test

import (
	"fmt"
	"testing"

	"gitlab.com/JankTech/hail"
)

func TestCountryCodeAlpha2(t *testing.T) {
	t.Parallel()
	for _, cc := range alpha2Pass {
		cc := cc
		t.Run(fmt.Sprintf(`%s passes`, cc), func(t *testing.T) {
			t.Parallel()
			cc := cc
			r, err := hail.Rule(hail.CountryCodeISO3166Alpha2())
			if err != nil {
				panic(err)
			}

			ok, errs := r.Validate(cc)
			if !ok {
				t.Fatalf("unexpected ok value: expected true, got false")
			}
			if len(errs) > 0 {
				t.Fatalf("incorrect validation error count: expected %d got %d", 0, len(errs))
			}
		})
	}

	t.Run("AA fails", func(t *testing.T) {
		t.Parallel()
		r, err := hail.Rule(hail.CountryCodeISO3166Alpha2())
		if err != nil {
			panic(err)
		}

		ok, errs := r.Validate("AA")
		if ok {
			t.Fatalf("unexpected ok value: expected false, got true")
		}
		if len(errs) != 1 {
			t.Fatalf("incorrect validation error count: expected %d got %d", 1, len(errs))
		}
	})
}

func TestCountryCodeAlpha3(t *testing.T) {
	t.Parallel()
	for _, cc := range alpha3Pass {
		cc := cc
		t.Run(fmt.Sprintf(`%s passes`, cc), func(t *testing.T) {
			t.Parallel()
			cc := cc
			r, err := hail.Rule(hail.CountryCodeISO3166Alpha3())
			if err != nil {
				panic(err)
			}

			ok, errs := r.Validate(cc)
			if !ok {
				t.Fatalf("unexpected ok value: expected true, got false")
			}
			if len(errs) > 0 {
				t.Fatalf("incorrect validation error count: expected %d got %d", 0, len(errs))
			}
		})
	}
}
