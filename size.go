package hail

import "fmt"

func Size[T ~string | ~[]S, S any](min uint, max uint) func(value T) error {
	return func(value T) error {
		if len(value) < int(min) || len(value) > int(max) {
			return ErrSize[T, S]{
				value: value,
				min:   min,
				max:   max,
			}
		}

		return nil
	}
}

type ErrSize[T ~string | ~[]S, S any] struct {
	value T
	min   uint
	max   uint
}

func NewErrSize[T ~string | ~[]S, S any](value T, min, max uint) ErrSize[T, S] {
	return ErrSize[T, S]{
		value: value,
		min:   min,
		max:   max,
	}
}

func (err ErrSize[T, S]) Error() string {
	return fmt.Sprintf("length of %v is not between %d and %d", err.value, err.min, err.max)
}
