package hail_test

import (
	"fmt"

	"gitlab.com/JankTech/hail"
)

func ExampleRule() {
	v := 123
	r, err := hail.Rule(hail.Min(130), hail.Max(110))
	if err != nil {
		panic(err)
	}

	ok, errs := r.Validate(v)
	if !ok {
		for _, err = range errs {
			fmt.Println(err.Error())
		}
	}

	// Output: value 123 is less than min 130
	// value 123 is more than max 110
}
