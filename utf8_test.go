package hail_test

import (
	"fmt"
	"testing"

	"gitlab.com/JankTech/hail"
)

func TestUTF8(t *testing.T) {
	t.Parallel()
	for _, a := range printableASCII {
		a := a
		t.Run(fmt.Sprintf(`%s passes`, a), func(t *testing.T) {
			t.Parallel()
			a := a
			r, err := hail.Rule(hail.PrintableUTF8())
			if err != nil {
				panic(err)
			}

			ok, errs := r.Validate(a)
			if !ok {
				t.Fatalf("unexpected ok value: expected true, got false")
			}
			if len(errs) > 0 {
				t.Fatalf("incorrect validation error count: expected %d got %d", 0, len(errs))
			}
		})
	}

	for _, a := range printableNonASCII {
		a := a
		t.Run(fmt.Sprintf("%s passes", a), func(t *testing.T) {
			t.Parallel()
			a := a
			r, err := hail.Rule(hail.PrintableUTF8())
			if err != nil {
				panic(err)
			}

			ok, errs := r.Validate(a)
			if !ok {
				t.Fatalf("unexpected ok value: expected true, got false")
			}
			if len(errs) > 0 {
				t.Fatalf("incorrect validation error count: expected %d got %d", 0, len(errs))
			}
		})
	}

	for _, a := range nonPrintableASCII {
		a := a
		t.Run(fmt.Sprintf("%x fails", a), func(t *testing.T) {
			t.Parallel()
			a := a
			r, err := hail.Rule(hail.PrintableUTF8())
			if err != nil {
				panic(err)
			}

			ok, errs := r.Validate(a)
			if ok {
				t.Fatalf("unexpected ok value: expected false, got true")
			}
			if len(errs) != 1 {
				t.Fatalf("incorrect validation error count: expected %d got %d", 1, len(errs))
			}
		})
	}
}
