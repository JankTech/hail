package hail

import (
	"errors"
	"fmt"
)

func PrintableUTF8() func(value string) error {
	// f := Regex(`^((?![\x00-\x20\x7F-\x9F]).)*$`)
	f := Regex(`^[ -~¡-ÿĀ-🝳]*$`)
	return func(value string) error {
		err := f(value)
		var errRegex ErrRegex
		if errors.As(err, &errRegex) {
			return ErrUTF8{value}
		}
		return nil
	}
}

type ErrUTF8 struct {
	value string
}

func NewErrUTF8(value string) ErrUTF8 {
	return ErrUTF8{value: value}
}

func (err ErrUTF8) Error() string {
	return fmt.Sprintf("value %s contains characters outside of the UTF-8 printable range", err.value)
}
