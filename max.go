package hail

import (
	"fmt"

	"golang.org/x/exp/constraints"
)

// The field under validation must be less than or equal to the maximum value.
func Max[T constraints.Ordered](max T) func(value T) error {
	return func(value T) error {
		if value > max {
			return ErrMax[T]{value: value, max: max}
		}
		return nil
	}
}

type ErrMax[T constraints.Ordered] struct {
	value T
	max   T
}

func NewErrMax[T constraints.Ordered](value T, max T) ErrMax[T] {
	return ErrMax[T]{
		value: value,
		max:   max,
	}
}

func (err ErrMax[T]) Error() string {
	return fmt.Sprintf("value %v is more than max %v", err.value, err.max)
}
