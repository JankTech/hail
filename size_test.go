package hail_test

import (
	"testing"

	"gitlab.com/JankTech/hail"
)

func TestSize(t *testing.T) {
	t.Parallel()
	t.Run("string with length between min and max passes", func(t *testing.T) {
		t.Parallel()

		v := "ABC"
		r, err := hail.Rule(hail.Size[string, string](1, 4))
		if err != nil {
			panic(err)
		}

		ok, errs := r.Validate(v)
		if !ok {
			t.Fatalf("unexpected ok value: expected true, got false")
		}
		if len(errs) > 0 {
			t.Fatalf("incorrect validation error count: expected %d got %d", 0, len(errs))
		}
	})

	t.Run("string with length equal to min and max passes", func(t *testing.T) {
		t.Parallel()

		v := "ABC"
		r, err := hail.Rule(hail.Size[string, string](3, 3))
		if err != nil {
			panic(err)
		}

		ok, errs := r.Validate(v)
		if !ok {
			t.Fatalf("unexpected ok value: expected true, got false")
		}
		if len(errs) > 0 {
			t.Fatalf("incorrect validation error count: expected %d got %d", 0, len(errs))
		}
	})

	t.Run("string with length shorter than min fails", func(t *testing.T) {
		t.Parallel()

		v := "ABC"
		r, err := hail.Rule(hail.Size[string, string](4, 5))
		if err != nil {
			panic(err)
		}

		ok, errs := r.Validate(v)
		if ok {
			t.Fatalf("unexpected ok value: expected true, got false")
		}
		if len(errs) != 1 {
			t.Fatalf("incorrect validation error count: expected %d got %d", 1, len(errs))
		}
	})

	t.Run("string with length larger than max fails", func(t *testing.T) {
		t.Parallel()

		v := "ABC"
		r, err := hail.Rule(hail.Size[string, string](1, 2))
		if err != nil {
			panic(err)
		}

		ok, errs := r.Validate(v)
		if ok {
			t.Fatalf("unexpected ok value: expected true, got false")
		}
		if len(errs) != 1 {
			t.Fatalf("incorrect validation error count: expected %d got %d", 1, len(errs))
		}
	})

	t.Run("slice with length between min and max passes", func(t *testing.T) {
		t.Parallel()

		v := []int{1, 2, 3}
		r, err := hail.Rule(hail.Size[[]int, int](1, 4))
		if err != nil {
			panic(err)
		}

		ok, errs := r.Validate(v)
		if !ok {
			t.Fatalf("unexpected ok value: expected true, got false")
		}
		if len(errs) > 0 {
			t.Fatalf("incorrect validation error count: expected %d got %d", 0, len(errs))
		}
	})

	t.Run("slice with length equal to min and max passes", func(t *testing.T) {
		t.Parallel()

		v := []int{1, 2, 3}
		r, err := hail.Rule(hail.Size[[]int, int](3, 3))
		if err != nil {
			panic(err)
		}

		ok, errs := r.Validate(v)
		if !ok {
			t.Fatalf("unexpected ok value: expected true, got false")
		}
		if len(errs) > 0 {
			t.Fatalf("incorrect validation error count: expected %d got %d", 0, len(errs))
		}
	})

	t.Run("slice with length shorter than min fails", func(t *testing.T) {
		t.Parallel()

		v := []int{1, 2, 3}
		r, err := hail.Rule(hail.Size[[]int, int](4, 5))
		if err != nil {
			panic(err)
		}

		ok, errs := r.Validate(v)
		if ok {
			t.Fatalf("unexpected ok value: expected true, got false")
		}
		if len(errs) != 1 {
			t.Fatalf("incorrect validation error count: expected %d got %d", 1, len(errs))
		}
	})

	t.Run("slice with length larger than max fails", func(t *testing.T) {
		t.Parallel()

		v := []int{1, 2, 3}
		r, err := hail.Rule(hail.Size[[]int, int](1, 2))
		if err != nil {
			panic(err)
		}

		ok, errs := r.Validate(v)
		if ok {
			t.Fatalf("unexpected ok value: expected true, got false")
		}
		if len(errs) != 1 {
			t.Fatalf("incorrect validation error count: expected %d got %d", 1, len(errs))
		}
	})
}
