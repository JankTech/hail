package hail

import (
	"fmt"

	"golang.org/x/exp/constraints"
)

// The field under validation must be more than or equal to the minimum value.
func Min[T constraints.Ordered](min T) func(value T) error {
	return func(value T) error {
		if value < min {
			return ErrMin[T]{value: value, min: min}
		}
		return nil
	}
}

type ErrMin[T constraints.Ordered] struct {
	value T
	min   T
}

func NewErrMin[T constraints.Ordered](value T, min T) ErrMin[T] {
	return ErrMin[T]{
		value: value,
		min:   min,
	}
}

func (err ErrMin[T]) Error() string {
	return fmt.Sprintf("value %v is less than min %v", err.value, err.min)
}
