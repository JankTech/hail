package hail

import (
	"fmt"
	"regexp"
)

// The field under validation must match the given regular expression.
func Regex(pattern string) func(value string) error {
	return func(value string) error {
		matched, err := regexp.MatchString(pattern, value)
		if err != nil {
			return err
		}
		if !matched {
			return ErrRegex{value: value, pattern: pattern}
		}
		return nil
	}
}

type ErrRegex struct {
	value   string
	pattern string
}

func NewErrRegex(value string, pattern string) ErrRegex {
	return ErrRegex{
		value:   value,
		pattern: pattern,
	}
}

func (err ErrRegex) Error() string {
	return fmt.Sprintf("value %s does not match the regext pattern %s", err.value, err.pattern)
}
