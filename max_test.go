package hail_test

import (
	"errors"
	"testing"

	"gitlab.com/JankTech/hail"
)

func TestIntegerMax(t *testing.T) {
	t.Parallel()
	t.Run("smaller than max passes", func(t *testing.T) {
		t.Parallel()

		v := 56
		r, err := hail.Rule(hail.Max(57))
		if err != nil {
			panic(err)
		}

		ok, errs := r.Validate(v)
		if !ok {
			t.Fatalf("unexpected ok value: expected true, got false")
		}
		if len(errs) > 0 {
			t.Fatalf("incorrect validation error count: expected %d got %d", 0, len(errs))
		}
	})

	t.Run("equal to max passes", func(t *testing.T) {
		t.Parallel()

		v := 56
		r, err := hail.Rule(hail.Max(56))
		if err != nil {
			panic(err)
		}

		ok, errs := r.Validate(v)
		if !ok {
			t.Fatalf("unexpected ok value: expected true, got false")
		}
		if len(errs) > 0 {
			t.Fatalf("incorrect validation error count: expected %d got %d", 0, len(errs))
		}
	})

	t.Run("larger than max fails", func(t *testing.T) {
		t.Parallel()

		v := 56
		r, err := hail.Rule(hail.Max(55))
		if err != nil {
			panic(err)
		}

		ok, errs := r.Validate(v)
		if ok {
			t.Fatalf("unexpected ok value: expected false, got true")
		}
		if len(errs) != 1 {
			t.Fatalf("incorrect validation error count: expected %d got %d", 1, len(errs))
		}
		var e hail.ErrMax[int]
		if !errors.As(errs[0], &e) {
			t.Fatalf("incorrect error type: expected %T, got %T", e, errs[0])
		}
	})
}

func TestFloatMax(t *testing.T) {
	t.Parallel()
	t.Run("smaller than max passes", func(t *testing.T) {
		t.Parallel()

		v := 0.23
		r, err := hail.Rule(hail.Max(0.24))
		if err != nil {
			panic(err)
		}

		ok, errs := r.Validate(v)
		if !ok {
			t.Fatalf("unexpected ok value: expected true, got false")
		}
		if len(errs) > 0 {
			t.Fatalf("incorrect validation error count: expected %d got %d", 0, len(errs))
		}
	})

	t.Run("equal to max passes", func(t *testing.T) {
		t.Parallel()

		v := 0.23
		r, err := hail.Rule(hail.Max(0.23))
		if err != nil {
			panic(err)
		}

		ok, errs := r.Validate(v)
		if !ok {
			t.Fatalf("unexpected ok value: expected true, got false")
		}
		if len(errs) > 0 {
			t.Fatalf("incorrect validation error count: expected %d got %d", 0, len(errs))
		}
	})

	t.Run("larger than max fails", func(t *testing.T) {
		t.Parallel()

		v := 0.23
		r, err := hail.Rule(hail.Max(0.22))
		if err != nil {
			panic(err)
		}

		ok, errs := r.Validate(v)
		if ok {
			t.Fatalf("unexpected ok value: expected false, got true")
		}
		if len(errs) != 1 {
			t.Fatalf("incorrect validation error count: expected %d got %d", 1, len(errs))
		}
		var e hail.ErrMax[float64]
		if !errors.As(errs[0], &e) {
			t.Fatalf("incorrect error type: expected %T, got %T", e, errs[0])
		}
	})
}
