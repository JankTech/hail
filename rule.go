package hail

import (
	"errors"
	"slices"
)

type RuleSet[T any] []func(value T) error

func Rule[T any](rules ...func(value T) error) (RuleSet[T], error) {
	return RuleSet[T](rules), nil
}

func (rs RuleSet[T]) Validate(value T) (valid bool, validationErrors []error) {
	errs := []error{}
	for _, r := range rs {
		err := r(value)
		if err != nil && !slices.ContainsFunc(errs, func(e error) bool {
			return errors.Is(e, err)
		}) {
			errs = append(errs, err)
		}
	}

	return len(errs) == 0, errs
}
