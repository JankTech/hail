package hail_test

import (
	"errors"
	"testing"

	"gitlab.com/JankTech/hail"
)

func TestIntegerMin(t *testing.T) {
	t.Parallel()
	t.Run("larger than min passes", func(t *testing.T) {
		t.Parallel()

		v := 56
		r, err := hail.Rule(hail.Min(55))
		if err != nil {
			panic(err)
		}

		ok, errs := r.Validate(v)
		if !ok {
			t.Fatalf("unexpected ok value: expected true, got false")
		}
		if len(errs) > 0 {
			t.Fatalf("incorrect validation error count: expected %d got %d", 0, len(errs))
		}
	})

	t.Run("equal to min passes", func(t *testing.T) {
		t.Parallel()

		v := 56
		r, err := hail.Rule(hail.Min(56))
		if err != nil {
			panic(err)
		}

		ok, errs := r.Validate(v)
		if !ok {
			t.Fatalf("unexpected ok value: expected true, got false")
		}
		if len(errs) > 0 {
			t.Fatalf("incorrect validation error count: expected %d got %d", 0, len(errs))
		}
	})

	t.Run("smaller than min fails", func(t *testing.T) {
		t.Parallel()

		v := 56
		r, err := hail.Rule(hail.Min(57))
		if err != nil {
			panic(err)
		}

		ok, errs := r.Validate(v)
		if ok {
			t.Fatalf("unexpected ok value: expected false, got true")
		}
		if len(errs) != 1 {
			t.Fatalf("incorrect validation error count: expected %d got %d", 1, len(errs))
		}
		var e hail.ErrMin[int]
		if !errors.As(errs[0], &e) {
			t.Fatalf("incorrect error type: expected %T, got %T", e, errs[0])
		}
	})
}

func TestFloatMin(t *testing.T) {
	t.Parallel()
	t.Run("larger than min passes", func(t *testing.T) {
		t.Parallel()

		v := 0.45
		r, err := hail.Rule(hail.Min(0.44))
		if err != nil {
			panic(err)
		}

		ok, errs := r.Validate(v)
		if !ok {
			t.Fatalf("unexpected ok value: expected true, got false")
		}
		if len(errs) > 0 {
			t.Fatalf("incorrect validation error count: expected %d got %d", 0, len(errs))
		}
	})

	t.Run("equal to min passes", func(t *testing.T) {
		t.Parallel()

		v := 0.45
		r, err := hail.Rule(hail.Min(0.45))
		if err != nil {
			panic(err)
		}

		ok, errs := r.Validate(v)
		if !ok {
			t.Fatalf("unexpected ok value: expected true, got false")
		}
		if len(errs) > 0 {
			t.Fatalf("incorrect validation error count: expected %d got %d", 0, len(errs))
		}
	})

	t.Run("smaller than min fails", func(t *testing.T) {
		t.Parallel()

		v := 0.45
		r, err := hail.Rule(hail.Min(0.46))
		if err != nil {
			panic(err)
		}

		ok, errs := r.Validate(v)
		if ok {
			t.Fatalf("unexpected ok value: expected false, got true")
		}
		if len(errs) != 1 {
			t.Fatalf("incorrect validation error count: expected %d got %d", 1, len(errs))
		}
		var e hail.ErrMin[float64]
		if !errors.As(errs[0], &e) {
			t.Fatalf("incorrect error type: expected %T, got %T", e, errs[0])
		}
	})
}
